<?php

use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\Task\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
 

$routeParam = 'id';

Route::get('test',function(){
    return "OK";
});

Route::group(['prefix'=>'auth'], function(){
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);

    Route::middleware('auth:api')->group(function () {
        Route::get('show', [AuthController::class, 'showAuth']);
    });
});


Route::group(
    [
        'prefix'=>'tasks',
        'middlewate'=> ['auth:api']
    ],
    function () use ($routeParam) {
        Route::get("/", [TaskController::class, 'index']);
        Route::post("/", [TaskController::class, 'store']);
        Route::post("/upload", [TaskController::class, 'upload']);
        Route::get("/{{$routeParam}}", [TaskController::class, 'show']);
        Route::patch("/{{$routeParam}}", [TaskController::class, 'edit']);
        Route::delete("/{{$routeParam}}", [TaskController::class, 'delete']);
        Route::post("/destroy/{{$routeParam}}", [TaskController::class, 'destroy']);
    }
);