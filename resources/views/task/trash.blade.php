@extends('layouts.frontend')

{{-- Page Title --}}
@section('page-title', 'Login')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Sign in to start your session')

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h5>Trash</h5>
                </div>
                
            </div>
            
        </div>
        <!-- ./col -->
        
<div class="card">
        <div class="card-header pull-right" style="padding-bottom: 7px;">
            <div class="input-group mb-3">
                <input type="text" class="form-control" id="searchText" name="searchText" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
            </div>
        </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="custom-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th><a href="#" id="order" class="sort-clickable" data-item-id="created_at">Created At</a></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id="table-lists">

            </tbody>
        </table>
    </div>
    <!-- /.card-body -->

    <div class="card-header pull-left">
                    <a href="#" data-toggle="modal" data-target="#addModal" class="btn btn-sm btn-primary margin-r-8 margin-l-8">
                                <i class="fa fa-plus"></i> <span>Add</span>
                            </a>
                </div>
</div>
@include('task.modals.delete')
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')

@endsection

@push('footer-scripts')
<script>
    jQuery(document).ready(function(){
        $.ajax({
        type: 'GET',
        url: '/api/tasks?is_trash=1',
        success: function(data){
            tableHtml = "";
            $.each( data, function( key, values ) {
                $.each( values, function( key, value ) {
                    console.log(value);
                    
                    actions = '<td>'+
                            '<a href="#" id="delete-item" data-item-id="'+value['id']+'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>'+
                        '</td>'
                    tableHtml += '<tr class="data-row">'+'<td>'+ value['id'] + '</td>' + '<td class="name">'+ value['name'] + '</td>' + '<td class="status">'+ value['status'] + '</td>' + '</td>' + '<td class="created_at">'+ value['created_at'] + '</td>' + actions;
                    tableHtml += '</tr>';
                });
            });
                $( '#table-lists' ).html( tableHtml );
            },
        error: function(data){
            var errors = data.responseJSON; 
            console.log(errors);
        }})
    });


  $(document).on('click', "#delete-item", function() {
        $(this).addClass('delete-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.    

        var options = {
          'backdrop': 'static'
        };
        $('#deleteModal').modal(options)
    });

    // on modal show
    $('#deleteModal').on('show.bs.modal', function() {
    var el = $(".delete-item-trigger-clicked"); // See how its usefull right here? 
    var row = el.closest(".data-row");

    // get the data
    var id = el.data('item-id');

    // fill the data in the input fields
    $("#delete-input-id").val(id);

  });

  // on modal hide
  $('#deleteModal').on('hide.bs.modal', function() {
    $('.delete-item-trigger-clicked').removeClass('delete-item-trigger-clicked')
    $("#deleteForm").trigger("reset");
  });

  
  // Delete
  $('#deleteForm').submit(function(e) {
    e.preventDefault();
    var id = $("#delete-input-id").val();
    $.ajax({
        type: 'POST',
        url: '/api/tasks/destroy/'+id,
        success: function(data){
            alert(data['message']);
            console.log(data);
            location.reload();
        },
        error: function(data){
            var errors = data.responseJSON; 
            console.log(errors);
        }})
  });


  $('#searchText').on('keyup',function(){
    $( '#table-lists').html("");
    var name = $('#searchText').val();
    $.ajax({
        type: 'GET',
        url: '/api/tasks?is_trash=1&name='+name,
        success: function(data){
            tableHtml = "";
            $.each( data, function( key, values ) {
                $.each( values, function( key, value ) {
                    img = '<img width="50" height="50" src="/uploads/tasks/images/'+value['image_file_name']+'">';
                    actions = '<td>'+
                            '<a href="#" id="delete-item" data-item-id="'+value['id']+'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>'+
                        '</td>'
                    tableHtml += '<tr class="data-row">'+'<td>'+ value['id'] + '</td>' + '<td class="name">'+img+ value['name'] + '</td>' + '<td class="status">'+ value['status'] + '</td>' + '</td>' + '<td class="created_at">'+ value['created_at'] + '</td>' + actions;
                    tableHtml += '</tr>';
                });
            });
                $( '#table-lists' ).html( tableHtml );
            },
        error: function(data){
            var errors = data.responseJSON; 
            console.log(errors);
    }})
  });

  $('#order').on('click',function(){
    $( '#table-lists').html("");
    var name = $('#searchText').val();
    var sort = $(this).data('item-id');
    if(sort.includes("-")){
        $(this).data('item-id','created_at');
    }else{
        $(this).data('item-id','-created_at');
    }
    var order = $(this).data('item-id');
    var url = '/api/tasks?is_trash=1&order_by='+order;

    if(name){
        url += '&name='+name;
    }
    
    $.ajax({
        type: 'GET',
        url: url,
        success: function(data){
            tableHtml = "";
            $.each( data, function( key, values ) {
                $.each( values, function( key, value ) {
                    img = '<img width="50" height="50" src="/uploads/tasks/images/'+value['image_file_name']+'">';
                    actions = '<td>'+
                            '<a href="#" id="delete-item" data-item-id="'+value['id']+'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>'+
                        '</td>'
                    tableHtml += '<tr class="data-row">'+'<td>'+ value['id'] + '</td>' + '<td class="name">'+img+ value['name'] + '</td>' + '<td class="status">'+ value['status'] + '</td>' + '</td>' + '<td class="created_at">'+ value['created_at'] + '</td>' + actions;
                    tableHtml += '</tr>';
                });
            });
                $( '#table-lists' ).html( tableHtml );
            },
        error: function(data){
            var errors = data.responseJSON; 
            console.log(errors);
    }})
  });

</script>
@endpush
