
<!-- Modal -->
<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form id="showForm" class="form-horizontal" method="PATCH" action="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-group" id="form-errors"></div>
      <div class="col-md-7">
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Name *</label>
                <input type="text" class="form-control hidden" id="show-input-id" name="id" placeholder="Id" required>
                <input type="text" class="form-control" id="show-input-name" name="name" placeholder="Name" required>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
        </div>

        <div class="col-md-7">
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Status *</label>
                <select class="form-control form-select" id="show-input-status" name="show-input-status" placeholder="Status">
                  @foreach(App\Task::STATUSES as $status)
                    <option value="{{$status}}">{{$status}}</option>
                  @endforeach
                </select>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="editTask" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>