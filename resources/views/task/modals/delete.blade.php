
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form id="deleteForm" class="form-horizontal" method="DELETE" action="">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    
      <input type="text" class="form-control hidden" id="delete-input-id" name="id" placeholder="Id" required>
        <div class="col-md-12">
                <label for="name">Are you sure ?</label>
        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="deleteTask" class="btn btn-primary">Delete</button>
      </div>
      </form>
    </div>
  </div>
</div>