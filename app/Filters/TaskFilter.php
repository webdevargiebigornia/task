<?php

namespace App\Filters;

use App\Task;

class TaskFilter
{
    public function apply($query)
    {
        if (request()->filled('is_trash')) {
            $query->onlyTrashed();
        }
        
        // Filter name and status
        if (request()->filled('name')) {
            $query->where('name', 'LIKE', request()->name."%");
        }

        if (request()->filled('status')) {
            $query->where('status', request()->status);
        }

        // Sort by asc and desc by adding or removing "-"
        if (request()->filled('order_by')) {
            $orderBy = str_replace('-', '', request()->order_by);
            if(in_array($orderBy, Task::SORTABLE_COLUMNS)){
                $orderType = str_contains(request()->order_by, "-") ? 'DESC' : 'ASC';
                
                $query->orderBy($orderBy, $orderType);
            } 
        }

        return $query;
    }
}
