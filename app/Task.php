<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'status', 'image_path', 'image_file_name'
    ];

    public const STATUS_TODO = "Todo";
    public const STATUS_IN_PROGRESS = "In Progress";
    public const STATUS_COMPLETED = "Completed";

    public const STATUSES = [
        self::STATUS_TODO => 'Todo',
        self::STATUS_IN_PROGRESS => 'In Progress',
        self::STATUS_COMPLETED => 'Completed',
    ];

    public const STATUS_DEFAULT = self::STATUS_TODO;

    public const SORTABLE_COLUMNS = [
        'created_at',
    ];

    public function scopeFilters($query, $filters)
    {
        return $filters->apply($query);
    }
}
