<?php

namespace App\Http\Controllers\API\Task;

use App\Filters\TaskFilter;
use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function index(TaskFilter $request){
        $task = Task::filters($request)->get();

        return response()->json(['data' => $task]);
    }

    public function store(Request $request){
        try {
            //TODO:: convert to rule and handle exception codes
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:1',
                'status' => 'sometimes|in:'.implode(",", Task::STATUSES)
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 422); 
            }
            
            $task = Task::create([
                'name' => $request->name,
                'status' => $request->status ?? Task::STATUS_DEFAULT
            ]);
            return response()->json(['message' => 'Successfully Added.']);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function show($id){
        try {
            $task = Task::find($id);

            return response()->json(['data' => $task]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function edit(Request $request, $id){
        try {
            //TODO:: convert to rule and handle exception codes
            $validator = Validator::make($request->all(), [
                'name' => 'sometimes|min:1',
                'status' => 'sometimes|in:'.implode(",", Task::STATUSES)
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 422); 
            }

            $task = Task::find($id);
            if($request && $task){
                $task->update($request->all());
                return response()->json(['message' => 'Successfully updated.']);
            }

            return response()->json(['message' => 'No changes.']);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function delete($id){
        try {

            $task = Task::find($id);

            if($task){
                $task->delete();
                return response()->json(['message' => 'Successfully deleted.']);
            }
            
            return response()->json(['message' => 'Not found.']);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function destroy($id){
        try {

            $task = Task::onlyTrashed()->find($id);

            if($task){
                $task->forceDelete();
                return response()->json(['message' => 'Successfully destroyed.']);
            }
            
            return response()->json(['message' => 'Not found.']);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function upload(Request $request){
        try {
            //TODO:: convert to rule and handle exception codes
            $validator = Validator::make($request->all(), [
                'id' => 'required|exists:tasks,id',
                'image' => 'required|image:jpeg,png',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 422); 
            }

            $file = $request->file('image');

            if(!$file->isValid()) {
                return response()->json(['invalid_file_upload'], 400);
            }

            $path = public_path() . '/uploads/tasks/images/';
            $name = $file->getClientOriginalName();
            $file->move($path, $name);

            $task = Task::find($request->id);
            $task->image_path = $path;
            $task->image_file_name = $name;

            $task->save();

            return response()->json(['data' => 'Successfully Uploaded.']);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

}
