<?php
 
namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        try {
            //TODO:: convert to rule and handle exception codes
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:1',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 422); 
            }

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
      
            $token = $user->createToken('token')->accessToken;
      
            return response()->json(['token' => $token], 200);

        } catch (\Throwable $th) {
            throw $th;
        }
        
    }
  
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
  
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('token')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
 
    public function showAuth() 
    {
 
     $user = auth()->user();
      
     return response()->json(['user' => $user], 200);
 
    }
}