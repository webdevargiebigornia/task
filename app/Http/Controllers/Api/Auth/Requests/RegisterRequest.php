<?php

namespace App\Http\Controllers\API\Auth\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:1',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'The name field is required',
            'email.required'  => 'The email field is required',
            'password.required'  => 'The email field is required'
        ];
    }
}
