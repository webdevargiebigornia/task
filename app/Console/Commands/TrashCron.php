<?php
    
namespace App\Console\Commands;

use App\Task;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TrashCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trash:cron';
     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
     
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::onlyTrashed()->whereDate('deleted_at', '<=', now()->subDays(30))->get();
        
        foreach($tasks as $task){
            $task->forceDelete();
            Log::info('Deleted', [$task]);
        }
        
    }
}